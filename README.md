# osu-wine
osu! install/run wrapper, forked by diamondburned.

### Confirmed working for numerous people
https://osu.ppy.sh/community/forums/topics/794952

### **WARNING** If `STAGING_AUDIO_DURATION` isn't working for you, add `export` in front of it!

## Instructions

Run `./install.sh` **as root/sudo**

## Uninstalling

Run `./install.sh uninstall` **as root/sudo**

## Updating

Recommended way: `./install.sh update`

Hacky way: `sudo cp ./osu-wine /usr/bin/osu-wine`

## Installing user config file

Run `cp /etc/osu-wine.conf ~/.osu-wine.conf` as user

## Extra options

- `osu-wine --winetricks [packages]` runs `winetricks` inside that `WINEPREFIX`
- `osu-wine --winecfg` opens `winecfg` dialog
- `marcRPC=true` in `~/.osu-wine.conf` enables Rich Presence support 
- `LOGGO=true` in `~/.osu-wine.conf` uses Loggo to log, which adds extra timestamps
- `osu-wine -ul` updates Loggo with the `LOGGO_JOB` ID (don't touch this unless instructed so)
- PooN's `wine-osu` instructions:
	- At the bottom of the `~/.osu-wine.conf` file, there are 2 variables:
		1. `export STAGING_AUDIO_DURATION` to change audio duration
		2. `PATH` to make osu! use PooN's Wine (you must NOT change this)
	- [More information at his blog](https://blog.thepoon.fr/osuLinuxAudioLatency/)
	- If the console returns something such as `line 145: 21628 Illegal instruction`, please re-compile Poon's Wine manually and install it.

## Troubleshooting

- Running `osu-wine` does nothing
    - To fix this, run `osu-wine --kill` a couple of times then wait a few seconds for osu! to die off
    - Run `osu-wine` again

## Credits

- [Original code](https://github.com/Nefelim4ag/osu-wine) by [Nefelim4ag](https://github.com/Nefelim4ag)
- [Wine RPC](https://github.com/Marc3842h/rpc-wine) by [Marc3842h](https://github.com/Marc3842h)
